package ecma.ai.usertoexel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserToExelApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserToExelApplication.class, args);
    }

}
