package ecma.ai.usertoexel.servise;

import ecma.ai.usertoexel.entity.Role;
import ecma.ai.usertoexel.entity.User;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
public class CreateService {
    private List<User> users = new ArrayList<>();

    //USERLARNI LISTGA QOSHIB OLAMIZ
    {
        users.add(User.builder()
                .role(Role.ADMIN)
                .firstName("Afsona")
                .phoneNumber("+998337820090")
                .lastName("Teshaboyeva")
                .middleName("Turdiqul")
                .build());
        users.add(User.builder()
                .role(Role.CUSTOMER)
                .firstName("Alisher")
                .lastName("Rustamov")
                .phoneNumber("+998337820090")
                .middleName("Farhodovich")
                .build());
        users.add(User.builder()
                .role(Role.ADMIN)
                .firstName("Tesha")
                .phoneNumber("+998337820090")
                .lastName("Boltayev")
                .middleName("Ketmonovich")
                .build());
        users.add(User.builder()
                .role(Role.CUSTOMER)
                .firstName("Ketmon")
                .phoneNumber("+998337820090")
                .lastName("Teshayev")
                .middleName("Boltaboyev")
                .build());
        users.add(User.builder()
                .role(Role.ADMIN)
                .firstName("Sanjar")
                .lastName("Umidjonov")
                .phoneNumber("+998337820090")
                .middleName("Xasanov")
                .build());
        users.add(User.builder()
                .role(Role.ADMIN)
                .firstName("Temur")
                .phoneNumber("+998337820090")
                .lastName("Azizov")
                .middleName("Bobabekov")
                .build());
    }

    public void get(HttpServletResponse resp) throws IOException {

        HashMap<String, Object[]> data = new HashMap<>();
        //TEPADA TURADIGAN QATOR
        data.put("0", new Object[]{"NUMBER", "FIRSTNAME", "LASTNAME", "MIDDLENAME", "PHONENUMBER", "ROLE"});

        //USERLARNI QOSHYABMIZ
        for (int i = 1; i < users.size(); i++) {
            data.put("" + i, new Object[]{i, users.get(i).getFirstName(), users.get(i).getLastName(), users.get(i).getMiddleName(), users.get(i).getPhoneNumber(), users.get(i).getRole().name()});
        }

        //FILE NI RESPONSEGA QOSHIB BERVORAMIZ
        resp.setContentType("application/vnd.ms-excel");
        FileInputStream fileInputStream = generate(data);
        FileCopyUtils.copy(fileInputStream, resp.getOutputStream());
    }

    private FileInputStream generate(HashMap<String, Object[]> data) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Employee Data");
        sheet.setDefaultColumnWidth(15);
        Set<String> keyset = data.keySet();
        int rownum = 0;

        for (String key : keyset) {
            //QATORLARNI OCHAMIZ
            Row row;
            row = ((XSSFSheet) sheet).createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;

            //OBJECTLAR ARRAYINI CELL GA QOYIB CHIQAMIZ
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(true);
                if (cellnum==1){
                    //USTUN UCHUN
                    cell.setCellStyle(createWarningColor(workbook));
                }else if (rownum==1){
                    //QATOR UCHUN
                    cell.setCellStyle(createWarningColor(workbook));
                }
                //AGAR ARRAYDAGI ELEMENT TYPE STRING BOLSA
                if (obj instanceof String)
                    cell.setCellValue((String) obj);

                    //AGAR ARRAYDAGI ELEMENT TYPE INTEGER BOLSA
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
            }
        }
        try {
            FileOutputStream out = new FileOutputStream(new File("file/file.xlsx"));
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FileInputStream(new File("file/file.xlsx"));
    }
    public CellStyle createWarningColor(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        return style;
    }

}
