package ecma.ai.usertoexel.controller;

import ecma.ai.usertoexel.servise.CreateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class CreateController {
    private final CreateService createService;

    @GetMapping
    public void getFile( HttpServletResponse resp) throws IOException {
        createService.get( resp);
    }
}
