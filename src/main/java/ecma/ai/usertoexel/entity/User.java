package ecma.ai.usertoexel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User {

    private String firstName;
    private String lastName;
    private String middleName;
    private String phoneNumber;
    @Enumerated(EnumType.STRING)
    private Role role;
}
